
package com.cupon.cupo.repository;

import com.cupon.cupo.model.Usuario;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author agath
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
    
    
    public Optional<Usuario> findByCorreo(String correo);
    
}
