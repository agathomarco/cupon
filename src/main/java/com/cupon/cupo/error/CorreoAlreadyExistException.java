
package com.cupon.cupo.error;

/**
 *
 * @author agath
 */
public class CorreoAlreadyExistException extends RuntimeException{

    public CorreoAlreadyExistException(String message) {
        super("El correo ya se encuentra registrado " +message);
    }
    
    
    
}
