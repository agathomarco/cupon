
package com.cupon.cupo.error;

/**
 *
 * @author agath
 */

public class UsuarioNotFoundException extends RuntimeException{
    
    
    public UsuarioNotFoundException(Long id) {
        super("No se encontro el usuarion con id "+ id);
    }
    
    
}
