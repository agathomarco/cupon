
package com.cupon.cupo.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author agath
 */
@ControllerAdvice
public class ErrorHandlerAdvice {
    
    @ResponseBody
    @ExceptionHandler(UsuarioNotFoundException.class)
    public ResponseEntity<Object> usuarioNotFoundException(UsuarioNotFoundException ex) {
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(ex.getMessage(), headers, HttpStatus.NOT_FOUND);
    } 
    
    
    @ResponseBody
    @ExceptionHandler(CorreoAlreadyExistException.class)
    public ResponseEntity<Object> emailAlreadyExist(CorreoAlreadyExistException ex) {
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        return new ResponseEntity<>(ex.getMessage(), headers, HttpStatus.BAD_REQUEST);
    } 
    
}
