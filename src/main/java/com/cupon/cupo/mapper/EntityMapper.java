
package com.cupon.cupo.mapper;

import java.util.List;

/**
 *
 * @author agath
 */
public interface EntityMapper <D, E> {

    E toEntity(D dto);

    D toDto(E entity);

    List <E> toEntity(List<D> dtoList);

    List <D> toDto(List<E> entityList);
    
}
