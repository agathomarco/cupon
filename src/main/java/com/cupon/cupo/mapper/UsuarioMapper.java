
package com.cupon.cupo.mapper;

import com.cupon.cupo.dto.UsuarioDTO;
import com.cupon.cupo.model.Usuario;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 *
 * @author agath
 */
@Mapper(componentModel = "spring")
public interface UsuarioMapper extends EntityMapper<UsuarioDTO, Usuario> {
    
    
    @Mapping(source = "referencia.nombreCompleto", target = "nombreReferencia")
    @Mapping(source = "referencia.id", target = "idReferencia")
    @Override        
    UsuarioDTO toDto(Usuario usuario);
    
    @Mapping(source = "idReferencia", target = "referencia.id")
    @Mapping(source = "password", target = "contrasenia")
    @Override
    Usuario toEntity(UsuarioDTO usuarioDTO);
    
    default Usuario fromId(Long id) {
        
        if(id == null)
            return null;
        
        Usuario usuario = new Usuario();
        usuario.setId(id);
        return usuario;
        
    }
    
}
