
package com.cupon.cupo.dto;

/**
 *
 * @author agath
 */
public enum CuponStatusEnum {
    
    DISPONIBLE,
    UTILIZADO,
    CADUCADO
    
}
