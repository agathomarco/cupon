
package com.cupon.cupo.rest;

import com.cupon.cupo.dto.UsuarioDTO;
import com.cupon.cupo.service.UsuarioService;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author agath
 */
@RestController
@RequestMapping("/api")
public class UsuarioResource {
    
    @Autowired
    private UsuarioService usuarioService;
    
    
    
    @PostMapping("/usuario")
    public ResponseEntity<UsuarioDTO> crearUsuario(@Validated @RequestBody UsuarioDTO usuarioDTO) {
        
        UsuarioDTO usuarioNuevo = usuarioService.guardar(usuarioDTO);
        
        return ResponseEntity.ok(usuarioNuevo);
    }
    
    
    @GetMapping("/usuario/{id}")
    public ResponseEntity<UsuarioDTO> obtenerUsuarioPorId(@NotNull @PathVariable Long id) {
        
        
        return ResponseEntity.ok(usuarioService.buscarPorId(id));
    }
    
    
    @GetMapping("/usuarios")
    public ResponseEntity<List<UsuarioDTO>> obtenerUsuarios(Pageable pageable) {
        
        Page<UsuarioDTO> usuarios = usuarioService.buscarTodos(pageable);
        
        return new ResponseEntity<>(usuarios.getContent(),HttpStatus.OK);
                
    }
    
    
    @DeleteMapping("/usuario/{id}")
    public ResponseEntity<Void> borrarUsuario(@NotNull @PathVariable Long id) {
        
        usuarioService.borrar(id);
        
        return ResponseEntity.ok().build();
        
    }
    
}
