package com.cupon.cupo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CupoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CupoApplication.class, args);
	}

}
