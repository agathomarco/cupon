
package com.cupon.cupo.service;

import com.cupon.cupo.dto.UsuarioDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author agath
 */
public interface UsuarioService {
    
    
    
    UsuarioDTO guardar(UsuarioDTO usuarioDTO);
    
    UsuarioDTO buscarPorId(Long id);
    
    Page<UsuarioDTO> buscarTodos(Pageable pageable);
    
    void borrar(Long id);
    
}
