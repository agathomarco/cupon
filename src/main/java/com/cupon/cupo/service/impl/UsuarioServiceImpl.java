
package com.cupon.cupo.service.impl;

import com.cupon.cupo.dto.UsuarioDTO;
import com.cupon.cupo.error.CorreoAlreadyExistException;
import com.cupon.cupo.error.UsuarioNotFoundException;
import com.cupon.cupo.mapper.UsuarioMapper;
import com.cupon.cupo.model.Usuario;
import com.cupon.cupo.repository.UsuarioRepository;
import com.cupon.cupo.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author agath
 */
@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired 
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioMapper usuarioMapper;
    
   
    
    @Override
    public UsuarioDTO guardar(UsuarioDTO usuarioDTO) {
        
        usuarioRepository.findByCorreo(usuarioDTO.getCorreo())
                .ifPresent((Usuario usuario) -> {
                    throw new CorreoAlreadyExistException(usuario.getCorreo());
                });
        
        Usuario usuario = usuarioMapper.toEntity(usuarioDTO);
        
        
        usuario.setReferencia(usuarioRepository
                .findById(usuarioDTO.getIdReferencia()).orElseGet(() -> null));
        
        usuario = usuarioRepository.save(usuario);
        return usuarioMapper.toDto(usuario);
        
    }

    @Override
    @Transactional(readOnly = true)
    public UsuarioDTO buscarPorId(Long id) {
        
        Usuario usuario = usuarioRepository.findById(id)
                .orElseThrow(() -> new UsuarioNotFoundException(id));
        return usuarioMapper.toDto(usuario);
        
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UsuarioDTO> buscarTodos(Pageable pageable) {
        
        return usuarioRepository.findAll(pageable).map(usuarioMapper::toDto);
    }

    @Override
    public void borrar(Long id) {
        usuarioRepository.deleteById(id);
        
    }

    
}
