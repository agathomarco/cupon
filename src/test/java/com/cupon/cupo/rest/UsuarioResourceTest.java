
package com.cupon.cupo.rest;

import com.cupon.cupo.dto.UsuarioDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Assert;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author agath
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UsuarioResourceTest {
    
    
    
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    private String url = "http://localhost:%d/api/%s";
    
    @Test
    public void crearStatus() throws Exception {
        
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setNombres("Juan");
        usuarioDTO.setApellidoPaterno("Perez");
        usuarioDTO.setApellidoMaterno("Martinez");
        usuarioDTO.setCorreo("correo@hotmail.com");
        usuarioDTO.setPassword("test");
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<UsuarioDTO> request = new HttpEntity<>(usuarioDTO, headers);
        ResponseEntity<UsuarioDTO> resultado = restTemplate.postForEntity(String.format(url, port, "usuario"), request, UsuarioDTO.class);
                
        
        Assert.assertEquals(200, resultado.getStatusCodeValue());
        
    }
    
    
    @Test
    public void crearId() throws Exception {
        
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setNombres("Juan");
        usuarioDTO.setApellidoPaterno("Perez");
        usuarioDTO.setApellidoMaterno("Martinez");
        usuarioDTO.setCorreo("correo1@hotmail.com");
        usuarioDTO.setPassword("test");
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>(usuarioDTO, headers);
        ResponseEntity<UsuarioDTO> resultado = restTemplate.postForEntity(String.format(url, port, "usuario"), request, UsuarioDTO.class);
                
        
        Assert.assertNotNull(resultado.getBody().getId());
        Assert.assertEquals("Juan", resultado.getBody().getNombres());
        Assert.assertEquals("Perez", resultado.getBody().getApellidoPaterno());
        Assert.assertEquals("Martinez", resultado.getBody().getApellidoMaterno());
        Assert.assertEquals("correo1@hotmail.com", resultado.getBody().getCorreo());
        
    }
}
